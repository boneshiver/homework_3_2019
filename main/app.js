const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')

const mysql = require('mysql2/promise')

const DB_USERNAME = 'root'
const DB_PASSWORD = 'welcome12#'

let conn

mysql.createConnection({
    user: DB_USERNAME,
    password: DB_PASSWORD
})
    .then((connection) => {
        conn = connection
        return connection.query('CREATE DATABASE IF NOT EXISTS tw_exam')
    })
    .then(() => {
        return conn.end()
    })
    .catch((err) => {
        console.warn(err.stack)
    })

const sequelize = new Sequelize('tw_exam', DB_USERNAME, DB_PASSWORD, {
    dialect: 'mysql',
    logging: false
})

let Student = sequelize.define('student', {
    name: Sequelize.STRING,
    address: Sequelize.STRING,
    age: Sequelize.INTEGER
}, {
    timestamps: false
})


const app = express()
app.use(bodyParser.json())

app.get('/create', async (req, res) => {
    try {
        await sequelize.sync({ force: true })
        for (let i = 0; i < 10; i++) {
            let student = new Student({
                name: 'name ' + i,
                address: 'some address on ' + i + 'th street',
                age: 30 + i
            })
            await student.save()
        }
        res.status(201).json({ message: 'created' })
    }
    catch (err) {
        console.warn(err.stack)
        res.status(500).json({ message: 'server error' })
    }
})

app.get('/students', async (req, res) => {
    try {
        let students = await Student.findAll()
        res.status(200).json(students)
    }
    catch (err) {
        console.warn(err.stack)
        res.status(500).json({ message: 'server error' })
    }
})

app.post('/students', async (req, res) => {
    let MISSING_BODY = { "message": "body is missing" };
    let MALFORMED_REQ = { "message": "malformed request" };
    let NEGATIVE_AGE = { "message": "age should be a positive number" };
    let CREATED = { "message": "created" };
    let keys = ['name', 'address', 'age'];
    let reqKeys = Object.keys(req.body);

    // Validation f for bad keys
    function badKeys() {
        if (reqKeys.length != 3) return true;
        else
            for (key of reqKeys)
                if (keys.indexOf(key) == -1)
                    return true;
        return false;
    }

    // async f to save a student from reqBody 
    async function saveStudent(reqBody) {
        await new Student({
            name: reqBody.name,
            address: reqBody.address,
            age: reqBody.age
        }).save();
    }

    try {
        // Actual logic:
        switch (true) {
            case !reqKeys.length:
                res.status(400).json(MISSING_BODY); break;
            case badKeys():
                res.status(400).json(MALFORMED_REQ); break;
            case req.body.age <= 0:
                res.status(400).json(NEGATIVE_AGE); break;
            default:
                saveStudent(req.body).then(res.status(201).json(CREATED));
        }
    }
    catch (err) {
        console.warn(err.stack)
        res.status(500).json({ message: 'server error' })
    }
})

module.exports = app